package org.example;

// The name of this class tells about the way it's written
public class MessyClass {
    String unUsedString;
    int unUsedInt;

    public String messIt(String a, String b, String c, String d, String e, String f) {
        Integer g = 2;
        g = 2;
        g = 2;
        g = 2;
        System.out.println(a);
        System.out.println(a);
        System.out.println(a);
        System.out.println(a);
        return a;
    }

    public void messItMore(String abc, String e) {
        if (e == abc) {
            System.out.println("That is a mess");
        } else if (e != abc) {
            System.out.println("That is a bigger mess");
        }
    }

    public void messItEvenMore(String abc, String e, String glk, String efg, String zxc) {
        if (e == abc) {
            System.out.println("That is a mess");
        } else if (e != abc) {
            System.out.println("That is a bigger mess");
        }
        if (e == abc) {
            System.out.println("That is a mess");
        } else if (e != abc) {
            System.out.println("That is a bigger mess");
        }
        if (e == abc) {
            System.out.println("That is a mess");
        } else if (e != abc) {
            System.out.println("That is a bigger mess");
        }
        if (e == abc) {
            System.out.println("That is a mess");
        } else if (e != abc) {
            System.out.println("That is a bigger mess");
        }
        if (e == abc) {
            System.out.println("That is a mess");
        } else if (e != abc) {
            System.out.println("That is a bigger mess");
        }
        if (e == abc) {
            System.out.println("That is a mess");
        } else if (e != abc) {
            System.out.println("That is a bigger mess");
        }
    }
}
