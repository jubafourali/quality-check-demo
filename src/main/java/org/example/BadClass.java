package org.example;

public class BadClass {
    String unUsedString;
    int unUsedInt;

    public String makeItBad(String a, String b, String c, String d, String e, String f) {
        String text = "The code is really bad";
        return text;
    }

    public String makeItWorse(String a, String b) {
        if (a == a) {
            System.out.println("That's obvious");
        } else if (a != a) {
            System.out.println("Oh no. That's not happening");
        }
        b = "That's a bad code";
        return b;
    }
}
