package org.example;

public class HelloWorld {

    // This is a code that should be caught by the quality code check tools
    public void sayHello(int number, String text, int randomParameter1) {
        for (int i=0; i<number; i++) {
            System.out.println(text);
        }
    }

    public static void main(String[] args) {
        System.out.println("Hello world!");
        MessyClass messyClass = new MessyClass();
        messyClass.messIt("", "", "", "", "", "");
        messyClass.messItMore("", "");
        messyClass.messItEvenMore("", "", "", "", "");
        BadClass badClass = new BadClass();
        badClass.makeItWorse("", "");
    }
}